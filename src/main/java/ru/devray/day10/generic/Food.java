package ru.devray.day10.generic;

public class Food extends Product {
    int callories;

    public Food(String company, int callories) {
        super(company);
        this.callories = callories;
    }
}
