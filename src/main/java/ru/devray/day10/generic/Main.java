package ru.devray.day10.generic;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static List<Cake> cakes = new ArrayList<>(List.of(
            new Cake("Красный октябрь", 1700, false, "Медовик"),
            new Cake("Бабаевский", 2100, false, "Графские развалины"),
            new Cake("ВкусВилл", 1200, false, "Мон блан"),
            new Cake("КФ Круг", 1700, false, "Тирольский пирог (персиковый)")
    ));

    public static List<Bread> breads = new ArrayList<>(List.of(
            new Bread("АО Тюменский хлебокомбинат", 600, 20),
            new Bread("Ржаной Край", 570, 12),
            new Bread("ТМ БулКо", 710, 28)
    ));

    public static List<Dessert> desserts = new ArrayList<>(List.of(
            new Dessert("МонБон", 220, true),
            new Dessert("Пират-Мармелад", 100, true),
            new Dessert("Яшкино", 280, true)
    ));

    public static List<Object> objects = new ArrayList<>(List.of(new Object()));

    //инвариантность, ковариантность, контрвариантность
    //хочешь записать что-то в дженерик? метод должен принимать КОНТРВАРИАНТНЫЙ дженерик
    //хочешь получить что-то из дженерика? метод должен принимать КОВАРИАНТНЫЙ дженерик
    //хочешь получить и записать? что-то в дженерик? метод должен принимать ИНВАРИАНТНЫЙ дженерик
    public static void main(String[] args) {
        Integer[] ints = {1,2,3};
        Object[] objects1 = ints;

        List<Integer> integers = List.of(1,2,3);
//        List<Object> objects2 = integers;
//        printAllCakesRecipe(desserts);
        List<Food> foodList = List.of(null);
//        calculateCallories(desserts);
        addDessertToList(desserts);
    }

    /**
     * Напечатать рецепты всех тортов
     */
    public static void printAllCakesRecipe(List<Cake> list) {
        list.stream().forEach(c -> System.out.println(c.recipe));
    }

    /**
     * Посчитать сумму каллорий
     * @return сумма каллорий
     */
    public static int calculateCallories(List<? extends Food> foods) {
        int sum = foods.stream().mapToInt(f -> f.callories).sum();
        System.out.println(sum);
        return sum;
    }

    /**
     * Добавить к списку дессерт
     */
    public static void addDessertToList(List<? super Dessert> list) {
        list.add(new Dessert("gsdg", 100, true));
    }

    /**
     * Вывести на экран все компании-производителя продуктов из списка
     */
    public static void printAllCompanyNames() {

    }

}
