package ru.devray.day10.generic;

public class Cake extends Dessert {
    String recipe;

    public Cake(String company, int callories, boolean isVegan, String recipe) {
        super(company, callories, isVegan);
        this.recipe = recipe;
    }
}
