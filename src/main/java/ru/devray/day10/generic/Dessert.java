package ru.devray.day10.generic;

public class Dessert extends Food {
    boolean isVegan;

    public Dessert(String company, int callories, boolean isVegan) {
        super(company, callories);
        this.isVegan = isVegan;
    }
}
