package ru.devray.day10.generic;

public class Bread extends Food {
    int bakeTime;

    public Bread(String company, int callories, int bakeTime) {
        super(company, callories);
        this.bakeTime = bakeTime;
    }
}
