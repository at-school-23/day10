package ru.devray.day10;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;

public class Main {
    public static void main(String[] args) {
        String searchQuery = "фильм Барби бесплатно full hd без регистрации";
        //apache-commons lang3
        boolean isPirate = StringUtils.containsAny(searchQuery, "халява", "бесплатно", "camrip");
        System.out.println(isPirate);


        Faker faker = new Faker();
        System.out.println(faker.name().firstName());
        System.out.println(faker.name().lastName());
        System.out.println(faker.phoneNumber().cellPhone());
        System.out.println(faker.internet().emailAddress());
        System.out.println(faker.internet().ipV4Address());
        System.out.println(faker.address().fullAddress());
    }
}
