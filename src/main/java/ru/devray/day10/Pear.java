package ru.devray.day10;

public class Pear {
    String name;

    public Pear(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pear{" +
                "name='" + name + '\'' +
                '}';
    }
}
